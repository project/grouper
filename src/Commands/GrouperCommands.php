<?php

namespace Drupal\grouper\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class GrouperCommands extends DrushCommands {

  /**
   * Creates a marker in the Log.
   *
   * @param string $marker_name
   *   The name of the new marker to create in the Log..
   *
   * @usage drush marker
   *   - Creates a marker in the Log.
   *
   * @command grouper:marker
   * @aliases marker mrk
   */
  public function marker($marker_name) {
    if (empty($marker_name)) {
      $marker_name = date("h:i : s a S");
    }
    else {
      $qty = \Drupal::database()->query("SELECT COUNT(*) FROM `watchdog` WHERE `type` = 'marker' AND `message` = :message", [":message" => 'MARKER - ' . $marker_name])->fetchField();

      if ($qty > 0) {
        print "\r\nThat Marker Name is Already Being Used. Please Choose a Unique Name.\r\n";
        return;
      }
    }

    \Drupal::logger('marker')->notice('MARKER - ' . $marker_name);
    $this->logger()->success("MARKER - `$marker_name` was created at: " . date("Y/m/d H:i:s"));
  }

  /**
   * Clears all the log entries below the specified log message.
   *
   * @param int $wid
   *   The wid to start clearing at.
   *
   * @usage drush watchdog-trim-before 1234
   *   - Clears all the log entries below wid 1234.
   *
   * @command grouper:watchdogTrimBefore
   * @aliases watchdog-trim-before wtb
   */
  public function watchdogTrimBefore($wid) {
    if (!empty($wid) && is_numeric($wid)) {
      if ($this->io()->confirm("  Are you sure you want to delete all log messages before wid $wid ?")) {

        db_query("DELETE FROM `watchdog` WHERE `wid` < :wid", ['wid' => $wid]);
        print "  Cleared messages below $arg1\r\n";
      }
    }
    else {
      print "\r\n  You need to pass in the numeric wid to delete before.\r\n";
    }
  }

  /**
   * Clears all the log entries above the supplied $wid.
   *
   * @param int $wid
   *   The wid to start clearing at.
   *
   * @usage drush watchdog-trim-after 1234
   *   - Clears all the log entries above wid 1234.
   *
   * @command grouper:watchdogTrimAfter
   * @aliases watchdog-trim-after wta
   */
  public function watchdogTrimAfter($wid) {
    if (!empty($wid) && is_numeric($wid)) {
      if ($this->io()->confirm("  Are you sure you want to delete all log messages after wid $wid ?")) {

        db_query("DELETE FROM `watchdog` WHERE `wid` > :wid", ['wid' => $wid]);
        $this->logger()->success("Cleared messages above $wid");
      }
    }
    else {
      print "\r\n  You need to pass in the numeric wid to delete after.\r\n";
    }
  }

  /**
   * Clears the log entries between the supplied $starting_wid and $ending_wid.
   *
   * @param int $starting_wid
   *   The wid to start clearing at.
   * @param int $ending_wid
   *   The wid to end clearing at.
   *
   * @usage drush marker-trim 1234 4321
   *   - Clears all the log entries between wid 1234 and wid 4321.
   *
   * @command grouper:markerTrimRange
   * @aliases watchdog-trim-range wtr
   */
  public function markerTrimRange($starting_wid = NULL, $ending_wid = NULL) {
    if (!empty($starting_wid) && is_numeric($starting_wid) && !empty($ending_wid) && is_numeric($ending_wid)) {
      if ($starting_wid < $ending_wid) {
        if ($this->io()->confirm("  Are you sure you want to delete all log messages between wid $starting_wid and wid $ending_wid ?")) {

          db_query("DELETE FROM `watchdog` WHERE `wid` > :swid AND  `wid` < :ewid", ['swid' => $starting_wid, 'ewid' => $ending_wid]);
          $this->logger()->success("Cleared messages between $starting_wid and $ending_wid.");
        }
      }
      else {
        print "\r\n  The ending-wid needs to be greater than the starting-wid.\r\n";
      }

    }
    else {
      print "\r\n  You need to pass in numeric wids to delete between.\r\n";
    }
  }

}
