<?php

namespace Drupal\grouper\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * AddMarkerForm.
 *
 * @author Seth Snyder
 */
class AddMarkerForm extends FormBase {

  /**
   * A {@inheritdoc}.
   */
  public function getFormId() {
    return 'email_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['header'] = [
      '#markup' => "<h2><b>Enter a name for the new marker</b></h2>",
    ];

    $form['marker-name'] = [
      '#type' => 'textfield',
      '#title' => 'New Marker Name',
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create a New Log Marker'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $form_values = $form_state->getValues();

    $marker_name = (isset($form_values['marker-name']) ? $form_values['marker-name'] : '');

    $qty = \Drupal::database()->query("SELECT COUNT(*) FROM `watchdog` WHERE `type` = 'marker' AND `message` = :message", [":message" => 'MARKER - ' . $marker_name])->fetchField();

    if ($qty > 0) {
      $form_state->setErrorByName('marker-name', $this->t('That Marker Name is Already Being Used. Please Choose a Unique Name.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $form_values = $form_state->getValues();

    $marker_name = (isset($form_values['marker-name']) ? $form_values['marker-name'] : '');

    \Drupal::logger('marker')->notice('MARKER - ' . $marker_name);

    $form_state->setRedirect($this->getParentRouteName());

  }

  /**
   * Gets the route name of the page behind the Modal.
   */
  public function getParentRouteName() {
    global $base_url;
    // Get the HTTP Referer.
    $referer = $_SERVER['HTTP_REFERER'];
    // Check for and remove the base url to return just the relative path.
    // Otherwise, return the referer as is.
    if (strpos($referer, $base_url) === 0) {
      $parent_path = substr($referer, strlen($base_url));
    }
    else {
      $parent_path = $referer;
    }

    // Get the route of the path.
    $url_object = \Drupal::service('path.validator')->getUrlIfValid($parent_path);
    $route_name = $url_object->getRouteName();

    return $route_name;

  }

}
