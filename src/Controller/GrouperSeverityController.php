<?php

namespace Drupal\grouper\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GrouperPhpSeverityController.
 */
class GrouperSeverityController extends ControllerBase {

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->database = $container->get('database');
    return $instance;
  }

  /**
   * Summary.
   *
   * @return string
   *   Return Hello string.
   */
  public function summary() {

    $header = [
      'Severity of Message',
      'PHP Qty',
      'Percentage',
      'Other Qty',
      'Percentage',
    ];
    $rows = [];

    $log_count = (int) $this->getLogCount('');
    $php_log_count = (int) $this->getLogCount('php');
    $rows[] = $this->formSeverityRow('Emergency', 0, $php_log_count, $log_count);
    $rows[] = $this->formSeverityRow('Alert', 1, $php_log_count, $log_count);
    $rows[] = $this->formSeverityRow('Critical', 2, $php_log_count, $log_count);
    $rows[] = $this->formSeverityRow('Error', 3, $php_log_count, $log_count);
    $rows[] = $this->formSeverityRow('Warning', 4, $php_log_count, $log_count);
    $rows[] = $this->formSeverityRow('Notice', 5, $php_log_count, $log_count);
    $rows[] = $this->formSeverityRow('Info', 6, $php_log_count, $log_count);
    $rows[] = $this->formSeverityRow('Debug', 7, $php_log_count, $log_count);

    $form['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    return $form;

  }

  /**
   * Formats the Whole Severity Row. All 5b Columns.
   */
  public function formSeverityRow($title, $severity, $php_log_count, $log_count) {
    $php_severity_count = (int) $this->getSeverityCount($severity, 'php');
    $severity_count = (int) $this->getSeverityCount($severity, '');

    return [$title,
      $this->formatLink($php_severity_count, 'php', $severity),
      number_format(($php_severity_count * 100 / $php_log_count), 2) . ' %',
      $this->formatLink($severity_count, '', $severity),
      number_format(($severity_count * 100 / $log_count), 2) . ' %',
    ];
  }

  /**
   * Gets the total count of php or non php messages.
   */
  public function getLogCount($type = '') {
    $query = $this->database->select('watchdog', 'w');
    if ($type == 'php') {
      $query->condition('w.type', 'php', '=');
    }
    else {
      $query->condition('w.type', 'php', '<>');
    }
    $count = $query->countQuery()->execute()->fetchField();
    return $count;
  }

  /**
   * Gets the severity count for a severity and type.
   */
  public function getSeverityCount($severity, $type = '') {
    $query = $this->database->select('watchdog', 'w');
    if ($type == 'php') {
      $query->condition('w.type', 'php', '=');
    }
    else {
      $query->condition('w.type', 'php', '<>');
    }
    $query->condition('w.severity', $severity);
    $count = $query->countQuery()->execute()->fetchField();
    return $count;
  }

  /**
   * Formats a Link.
   */
  public function formatLink($title, $type, $severity) {
    if (strcmp($type, 'php') == 0) {
      $target_path = '/admin/reports/grouper/php-summary/?severity=' . $severity;
    }
    else {
      $target_path = '/admin/reports/grouper/summary/?severity=' . $severity;
    }
    $link = "<a href=\"$target_path\" class=\"message-link\" title=\"Click For Summary\"target=\"_blank\">$title</a>";
    return ['data' => ['#markup' => $link]];
  }

}
