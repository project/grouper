<?php

namespace Drupal\grouper\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GrouperPeopleController.
 */
class GrouperPeopleController extends ControllerBase {

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->database = $container->get('database');
    return $instance;
  }

  /**
   * Summary.
   *
   * @return string
   *   Return Hello string.
   */
  public function summary() {

    $connection = $this->database;

    $header = [
      ['data' => $this->t('Count'), 'field' => 'count', 'sort' => 'desc'],
      ['data' => $this->t('Name'), 'field' => 'name'],
      ['data' => $this->t('EMail'), 'field' => 'mail'],
    ];

    $count_query = $connection->select('watchdog');
    $count_query->addExpression('COUNT(DISTINCT(message))');

    $query = $connection->select('watchdog', 'w')
      ->extend('\Drupal\Core\Database\Query\PagerSelectExtender')
      ->extend('\Drupal\Core\Database\Query\TableSortExtender');
    $query->addExpression('COUNT(wid)', 'count');

    $query->join('users_field_data', 'ufd', 'w.uid = ufd.uid');

    $query->fields('w', ['uid']);
    $query->fields('ufd', ['name', 'mail']);

    if (isset($_GET['type']) && !empty($_GET['type']) && $_GET['type'] == 'other') {
      $query->condition('w.type', 'php', '!=');
    }
    else {
      $query->condition('w.type', 'php', '=');
    }

    $query->groupBy('uid');
    $query->groupBy('name');
    $query->groupBy('mail');
    $query->distinct();
    $query->orderByHeader($header);
    $query->setCountQuery($count_query);
    $query->limit(30);

    /*
    // For Debugging.
    \Drupal::messenger()->addStatus($query->__toString());
    \Drupal::messenger()->addStatus(print_r($query->arguments(),1));
     */

    $result = $query->execute();

    $rows = [];
    foreach ($result as $row) {
      $rows[] = [
        $this->formatQuantity($row),
        $this->formatName($row, FALSE),
        $this->formatName($row, TRUE),
      ];
    }

    $build['pre-table'] = [
      '#children' => $this->renderFilterForm(),
    ];

    $build['dblog_top_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No log messages available.'),
      '#attached' => [
        'library' => ['dblog/drupal.dblog'],
      ],
    ];
    $build['dblog_top_pager'] = ['#type' => 'pager'];

    return $build;

  }

  /**
   * Formats a quantity message.
   *
   * @param object $row
   *   The record from the watchdog table.
   *   The object properties are: message, variables, count.
   */
  public function formatQuantity($row) {
    if (isset($_GET['type']) && !empty($_GET['type']) && $_GET['type'] == 'other') {
      $target_path = "/admin/reports/grouper/summary/" . '?uid=' . $row->uid;
    }
    else {
      $target_path = "/admin/reports/grouper/php-summary" . '?uid=' . $row->uid;
    }

    $link = "<a href=\"$target_path\" class=\"nid-link\" target=\"_blank\">$row->count</a>";
    return ['data' => ['#markup' => $link]];
  }

  /**
   * Formats a NAME message.
   *
   * @param object $row
   *   The record from the watchdog table.
   * @param bool $email
   *   Get the data from mail.
   */
  public function formatName($row, $email = FALSE) {
    if (isset($_GET['type']) && !empty($_GET['type']) && $_GET['type'] == 'other') {
      $target_path = "/admin/reports/grouper/summary/$wid/" . '?uid=' . $row->uid;
    }
    else {
      $target_path = "/admin/reports/grouper/php-summary/" . '?uid=' . $row->uid;
    }

    if ($email == TRUE) {
      if ($row->uid == 0 || $row->uid == '0') {
        $link = "<a href=\"$target_path\" class=\"nid-link\" target=\"_blank\">Anonymous</a>";
      }
      else {
        $link = "<a href=\"$target_path\" class=\"nid-link\" target=\"_blank\">$row->mail</a>";
      }
    }
    else {
      if ($row->uid == 0 || $row->uid == '0') {
        $link = "<a href=\"$target_path\" class=\"nid-link\" target=\"_blank\">Anonymous</a>";
      }
      else {
        $link = "<a href=\"$target_path\" class=\"nid-link\" target=\"_blank\">$row->name</a>";
      }
    }
    return ['data' => ['#markup' => $link]];
  }

  /**
   * Renders the Filters Form.
   *
   * @return string
   *   Return Error Type select list markup.
   */
  public function renderFilterForm() {
    $form_markup = '<form action="" METHOD="GET">';

    $form_markup .= '<table style="max-width: 400px;">';
    $form_markup .= '<tr>';
    $form_markup .= '<td>' . $this->renderTypeSelect(isset($_GET['type']) && !empty($_GET['type']) ? $_GET['type'] : '') . '</td>';
    $form_markup .= '<td valign="bottom"><input id="filter-submit" type="submit" value="Filter" size="12" style="height: 22px; padding-left: 10px; padding-right: 10px;"></td>';
    $form_markup .= '<td valign="bottom"><input type="reset" value="reset" style="height: 22px; padding-left: 10px; padding-right: 10px;"></td>';
    $form_markup .= '</tr>';
    $form_markup .= '</table>';
    $form_markup .= '</form>';
    return $form_markup;
  }

  /**
   * Type Select.
   *
   * @return string
   *   Return Type.
   */
  public function renderTypeSelect($type = 'PHP') {
    $types_arr = [
      'PHP',
      'other',
    ];

    $markup = '<label for="select-type">Type</label>
          <select data-drupal-selector="edit-severity"
                  name="type"
                  id="select-type"
                  size="1"
                  class="form-select">';

    foreach ($types_arr as $key => $arr_type) {
      if ($type == $arr_type) {
        $markup .= "<option selected=\"1\" value=\"$arr_type\">$arr_type</option>";
      }
      else {
        $markup .= "<option value=\"$arr_type\">$arr_type</option>";
      }
    }

    $markup .= '</select></span>';
    return $markup;

  }

}
