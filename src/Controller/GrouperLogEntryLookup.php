<?php

namespace Drupal\grouper\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Routing\RedirectDestination;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Url;
use Drupal\path_alias\AliasManager;

/**
 * Returns responses for experimental routes.
 */
class GrouperLogEntryLookup extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Lookup the log entry response.
   */
  public function lookup($wid) {

    if (!is_numeric($wid)) {
      return;
    }

    $target_wid = (int) $wid;

    // Determine the $min_wid.
    $query = $this->connection->select('watchdog', 'w');
    $query->addExpression('COUNT(*)', 'wid');
    $query->condition('w.wid', $target_wid, '>=');
    $position = (int) $query->execute()->fetchField();

    // Number of pages from the END.
    $page_num = intdiv($position, 50);
    $item_number = $position % 50;

    $redirectUrl = "/admin/reports/dblog/?page=$page_num&_ITEM-NUMBER=$item_number";

    $response = new TrustedRedirectResponse($redirectUrl);
    $response->send();

    // Need this ??.
    $build['content'] = [
      '#type' => 'item',
      '#markup' => '',
    ];

    return $build;

  }

}
