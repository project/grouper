<?php

namespace Drupal\grouper\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GrouperController.
 */
class GrouperController extends ControllerBase {

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->database = $container->get('database');
    return $instance;
  }

  /**
   * Shows the most frequent log messages of a given event type.
   *
   * Messages are not truncated on this page because events detailed herein do
   * not have links to a detailed view.
   *
   * @return array
   *   A build array in the format expected by
   *   \Drupal\Core\Render\RendererInterface::render().
   */
  public function summary() {
    $connection = $this->database;

    $header = [
      ['data' => $this->t('Count'), 'field' => 'count', 'sort' => 'desc'],
      ['data' => $this->t('Type'), 'field' => 'type'],
      ['data' => $this->t('Message'), 'field' => 'message'],
    ];

    $count_query = $connection->select('watchdog');
    $count_query->addExpression('COUNT(DISTINCT(message))');

    $query = $connection->select('watchdog', 'w')
      ->extend('\Drupal\Core\Database\Query\PagerSelectExtender')
      ->extend('\Drupal\Core\Database\Query\TableSortExtender');

    $query->addExpression('COUNT(wid)', 'count');
    $query->addExpression('MIN(wid)', 'wid');

    $query->fields('w', ['message', 'variables', 'type']);

    if (isset($_GET['type'])) {
      $query->condition('w.type', $_GET['type']);
    }

    if (isset($_GET['severity'])) {
      $query->condition('w.severity', $_GET['severity']);
    }

    if (isset($_GET['begin-m-wid']) && !empty($_GET['begin-m-wid']) && is_numeric($_GET['begin-m-wid'])) {
      $query->condition('w.wid', $_GET['begin-m-wid']);
    }

    if (isset($_GET['begin-wid']) && !empty($_GET['begin-wid']) && is_numeric($_GET['begin-wid'])) {
      $query->condition('w.wid', $_GET['begin-wid'], '>=');
    }

    if (isset($_GET['after']) && !empty($_GET['after']) && is_numeric($_GET['after'])) {
      $query->condition('w.timestamp', $_GET['after'], '>=');
    }

    if (isset($_GET['end-m-wid']) && !empty($_GET['end-m-widd']) && is_numeric($_GET['end-m-widd'])) {
      $query->condition('w.wid', $_GET['end-m-wid'], '<=');
    }

    if (isset($_GET['end-wid']) && !empty($_GET['end-wid']) && is_numeric($_GET['end-wid'])) {
      $query->condition('w.wid', $_GET['end-wid'], '<=');
    }

    if (isset($_GET['before']) && !empty($_GET['before']) && is_numeric($_GET['before'])) {
      $query->condition('w.timestamp', $_GET['before'], '<=');
    }

    if (isset($_GET['limit']) && !empty($_GET['limit']) && is_numeric($_GET['limit'])) {
      $query->limit($_GET['limit']);
    }
    else {
      $query->limit(30);
    }

    if (isset($_GET['user']) && !empty($_GET['user']) && is_numeric($_GET['user'])) {
      $query->condition('w.uid', $_GET['user'], '=');
    }

    if (isset($_GET['uid']) && !empty($_GET['uid']) && is_numeric($_GET['uid'])) {
      $query->condition('w.uid', $_GET['uid'], '=');
    }

    if (isset($_GET['location'])) {
      $query->condition('w.location', $_GET['location']);
    }

    if (isset($_GET['ip'])) {
      $query->condition('w.hostname', $_GET['ip']);
    }

    // Base Condition exclude php messages.
    $query->condition('w.type', 'php', '!=');

    $query->groupBy('message');
    $query->groupBy('variables');
    $query->groupBy('type');
    $query->distinct();
    $query->orderByHeader($header);
    $query->setCountQuery($count_query);

    /*
    \Drupal::messenger()->addStatus($query->__toString());
    \Drupal::messenger()->addStatus(print_r($query->arguments(),1));
     */

    $result = $query->execute();

    $rows = [];
    foreach ($result as $dblog) {
      if ($message = $this->formatMessage($dblog)) {
        $rows[] = [
          $this->formatQuantity($dblog),
          $this->formatType($dblog),
          $message,
        ];
      }
    }

    $build['pre-table'] = [
      '#children' => $this->renderFilterForm(),
    ];

    $build['dblog_top_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No log messages available.'),
      '#attached' => [
        'library' => ['dblog/drupal.dblog'],
      ],
    ];
    $build['dblog_top_pager'] = ['#type' => 'pager'];

    return $build;
  }

  /**
   * Formats a quantity message.
   *
   * @param object $row
   *   The record from the watchdog table.
   *   The object properties are: message, variables, count.
   */
  public function formatQuantity($row) {
    $request = \Drupal::request();
    $query_string = $request->getQueryString();
    if (empty($query_string)) {
      $query_string = '';
    }
    else {
      $query_string = '?' . $query_string;
    }

    $wid = $row->wid;
    $target_path = "/admin/reports/grouper/detail/$wid/" . $query_string;

    $link = "<a href=\"$target_path\" class=\"nid-link\" target=\"_blank\">$row->count</a>";
    return ['data' => ['#markup' => $link]];
  }

  /**
   * Formats a type message.
   *
   * @param object $row
   *   The record from the watchdog table.
   *   The object properties are: message, variables, count.
   */
  public function formatType($row) {
    $request = \Drupal::request();
    $query_string = $request->getQueryString();

    if (str_contains($row->type, ' ')) {
      $type = str_replace(' ', '+', $row->type);
    }
    else {
      $type = $row->type;
    }

    if (empty($query_string)) {
      $query_string = '?type=' . $type;
    }
    else {
      $query_string = '?type=' . $type . '&' . $query_string;
    }

    $target_path = "/admin/reports/grouper/summary/" . $query_string;

    $link = "<a href=\"$target_path\" class=\"nid-link\" target=\"_blank\">$row->type</a>";
    return ['data' => ['#markup' => $link]];
  }

  /**
   * Formats a database log message.
   *
   * @param object $row
   *   The record from the watchdog table. The object properties are: wid, uid,
   *   severity, type, timestamp, message, variables, link, name.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup|false
   *   The formatted log message or FALSE if the message or variables properties
   *   are not set.
   */
  public function formatMessage($row) {
    // Check for required properties.
    if (isset($row->message, $row->variables)) {
      $variables = @unserialize($row->variables);
      // Messages without variables or user specified text.
      if ($variables === NULL) {
        $message = Xss::filterAdmin($row->message);
      }
      elseif (!is_array($variables)) {
        $message = $this->t('Log data is corrupted and cannot be unserialized: @message', ['@message' => Xss::filterAdmin($row->message)]);
      }
      // Message to translate with injected variables.
      else {
        $message = $this->t(Xss::filterAdmin($row->message), $variables);
      }
    }
    else {
      $message = FALSE;
    }
    return $message;
  }

  /**
   * Renders the Filters Form.
   *
   * @return string
   *   Return Error Type select list markup.
   */
  public function renderFilterForm() {
    $form_markup = '<form action="" METHOD="GET">';

    $form_markup .= '<table style="max-width: 400px;">';
    $form_markup .= '<tr><td>' . $this->renderMarkerSelect('Begin Marker', 'begin-m-wid', (isset($_GET['begin-m-wid']) && !empty($_GET['begin-m-wid']) && is_numeric($_GET['begin-m-wid']) ? $_GET['begin-m-wid'] : '')) . '</td>';
    $form_markup .= '<td>' . $this->renderMarkerSelect('End Marker', 'end-m-wid', (isset($_GET['end-m-wid']) && !empty($_GET['end-m-wid']) && is_numeric($_GET['end-m-wid']) ? $_GET['end-m-wid'] : '')) . '</td>';
    $form_markup .= '<td>' . $this->renderTypeSelect(isset($_GET['type']) && !empty($_GET['type']) ? $_GET['type'] : '') . '</td>';
    $form_markup .= '<td>' . $this->renderSeveritySelect(isset($_GET['severity']) && !empty($_GET['severity']) && is_numeric($_GET['severity']) ? $_GET['severity'] : '-1') . '</td>';
    $form_markup .= '<td valign="bottom"><input id="filter-submit" type="submit" value="Filter" size="12" style="height: 22px; padding-left: 10px; padding-right: 10px;"></td>';
    $form_markup .= '<td valign="bottom"><input type="reset" value="reset" style="height: 22px; padding-left: 10px; padding-right: 10px;"></td>';
    $form_markup .= '<td valign="bottom"><a class="use-ajax button" data-accepts="application/vnd.drupal-modal" data-dialog-type="modal" href="/admin/grouper/add-marker?@query" style="width: 121px; border-radius: 5px; font-size: 12px;">Add Marker</a></td><tr>';

    $form_markup .= '<tr>';

    $form_markup .= '<td valign="bottom"><label for="begin-wid" style="display: inline;">Begining Event ID</label><input id="begin-wid" name="begin-wid" type="number" value="';
    $form_markup .= (isset($_GET['begin-wid']) && !empty($_GET['begin-wid']) && is_numeric($_GET['begin-wid']) ? $_GET['begin-wid'] : '');
    $form_markup .= '" style="width: 163px; display: inline;  margin-left: 5px;"></td>';

    $form_markup .= '<td valign="bottom"><label for="end-wid" style="display: inline;">Ending Event ID</label><input id="end-wid" name="end-wid" type="number" value="';
    $form_markup .= (isset($_GET['end-wid']) && !empty($_GET['end-wid']) && is_numeric($_GET['end-wid']) ? $_GET['end-wid'] : '');
    $form_markup .= '" style="display: inline; margin-left: 5px;"></td>';

    $form_markup .= '<td></td><td></td>';
    $form_markup .= '<td valign="bottom" colspan="3"><label for="limit" style="display: inline;">Limit</label><input id="limit" name="limit" type="number" value="';
    $form_markup .= (isset($_GET['limit']) && !empty($_GET['limit']) && is_numeric($_GET['limit']) ? $_GET['limit'] : '30');
    $form_markup .= '" style="display: inline; margin-left: 5px; "></td>';
    $form_markup .= '</tr>';

    $form_markup .= '</table>';
    $form_markup .= '</form>';
    return $form_markup;
  }

  /**
   * Error Type Selects.
   *
   * @return string
   *   Return Error Type select list markup.
   */
  public function renderTypeSelect($type = NULL) {

    $database = \Drupal::database();
    $query = $database->query("SELECT DISTINCT `type` FROM {watchdog} WHERE `type` != 'php'");
    $result = $query->fetchAll();

    $markup = '<label for="select-type">Type</label>
             <select data-drupal-selector="edit-severity"
                     name="type"
                     id="select-type"
                     size="8"
                     class="form-select">';

    foreach ($result as $typeObj) {
      if ($type == $typeObj->type) {
        $markup .= "<option selected=\"1\" value=\"$typeObj->type\">$typeObj->type</option>";
      }
      else {
        $markup .= "<option value=\"$typeObj->type\">$typeObj->type</option>";
      }
    }

    $markup .= '</select></span>';
    return $markup;
  }

  /**
   * Error Marker Selects.
   *
   * $title   Human Title of Select Box.
   * $name    Machine name of form element.
   *
   * @return string
   *   Return Error Type select list markup.
   */
  public function renderMarkerSelect($title, $name, $value = NULL) {

    $database = \Drupal::database();
    $query = $database->query("SELECT `wid`,`timestamp`,`message` FROM `watchdog` WHERE `type` = 'marker' ORDER BY `wid` ASC");
    $result = $query->fetchAll();

    $markup = '<label for="select-type">' . $title . '</label>
             <select data-drupal-selector="edit-severity"
                     name="' . $name . '"
                     id="select-type"
                     size="8"
                     class="form-select" 
                     style="width: 300px;">';

    foreach ($result as $typeObj) {
      if ($value == $typeObj->wid) {
        $markup .= "<option selected=\"1\" value=\"$typeObj->wid\" title=\"$typeObj->message\">$typeObj->message</option>";
      }
      else {
        $markup .= "<option value=\"$typeObj->wid\" title=\"$typeObj->message\">$typeObj->message</option>";
      }
    }

    $markup .= '</select></span>';
    return $markup;
  }

  /**
   * Severity Selects.
   *
   * @return string
   *   Return Severity Select List Markup.
   */
  public function renderSeveritySelect($severity = NULL) {
    $severities_arr = [
      'Emergency',
      'Alert',
      'Critical',
      'Error',
      'Warning',
      'Notice',
      'Info',
      'Debug',
    ];

    $markup = '<label for="select-severity">Severity</label>
          <select data-drupal-selector="edit-severity"
                  name="severity"
                  id="select-severity"
                  size="8"
                  class="form-select">';

    foreach ($severities_arr as $key => $severity_level) {
      if ($severity == $key) {
        $markup .= "<option selected=\"1\" value=\"$key\">$severity_level</option>";
      }
      else {
        $markup .= "<option value=\"$key\">$severity_level</option>";
      }
    }

    $markup .= '</select></span>';
    return $markup;

  }

  /**
   * Detail.
   *
   * @return string
   *   Return Hello string.
   */
  public function detail($wid) {

    $sub_query = $this->database->select('watchdog', 'w');
    $sub_query->fields('w', ['variables']);
    $sub_query->condition('w.wid', $wid);

    $query = $this->database->select('watchdog', 'w');
    $query->extend('\Drupal\Core\Database\Query\PagerSelectExtender');
    $query->extend('\Drupal\Core\Database\Query\TableSortExtender');

    $query->fields('w', ['wid', 'timestamp', 'location', 'referer']);
    $query->condition('w.variables', $sub_query);
    $query->distinct();

    $query->condition('w.type', 'php', '!=');

    // Filters out log messages.
    if (isset($_GET['severity'])) {
      $query->condition('w.severity', $_GET['severity']);
    }

    if (isset($_GET['begin-m-wid']) && !empty($_GET['begin-m-wid']) && is_numeric($_GET['begin-m-wid'])) {
      $query->condition('w.wid', $_GET['begin-m-wid']);
    }

    if (isset($_GET['begin-wid']) && !empty($_GET['begin-wid']) && is_numeric($_GET['begin-wid'])) {
      $query->condition('w.wid', $_GET['begin-wid'], '>=');
    }

    if (isset($_GET['after']) && !empty($_GET['after']) && is_numeric($_GET['after'])) {
      $query->condition('w.timestamp', $_GET['after'], '>=');
    }

    if (isset($_GET['end-m-wid']) && !empty($_GET['end-m-widd']) && is_numeric($_GET['end-m-widd'])) {
      $query->condition('w.wid', $_GET['end-m-wid'], '<=');
    }

    if (isset($_GET['end-wid']) && !empty($_GET['end-wid']) && is_numeric($_GET['end-wid'])) {
      $query->condition('w.wid', $_GET['end-wid'], '<=');
    }

    if (isset($_GET['before']) && !empty($_GET['before']) && is_numeric($_GET['before'])) {
      $query->condition('w.timestamp', $_GET['before'], '<=');
    }

    if (isset($_GET['user']) && !empty($_GET['user']) && is_numeric($_GET['user'])) {
      $query->condition('w.uid', $_GET['user'], '=');
    }

    if (isset($_GET['uid']) && !empty($_GET['uid']) && is_numeric($_GET['uid'])) {
      $query->condition('w.uid', $_GET['uid'], '=');
    }

    if (isset($_GET['location'])) {
      $query->condition('w.location', $_GET['location']);
    }

    if (isset($_GET['ip'])) {
      $query->condition('w.hostname', $_GET['ip']);
    }

    // The Quantity.
    $qty = $query->countQuery()->execute()->fetchField();

    // The Common Message.
    $detail_query = $this->database->select('watchdog', 'w');
    $detail_query->fields('w', ['message', 'variables', 'type'])
      ->condition('w.wid', $wid);
    $row = $detail_query->execute()->fetchAll();

    $type = ($row[0]->type ?? '');

    $quantity = '<div id="php-quantity-type">' . $qty . '&nbsp;&nbsp;' . $type . '</div>';

    $msg = ($this->formatMessage($row[0]) ?? '');
    $message = '<div id="php-message"><pre>' . $msg . '</pre></div>';

    $header = [
                 ['data' => $this->t('Date')],
                 ['data' => $this->t('Message')],
                 ['data' => $this->t('Lookup')],
    ];

    $rows = [];

    // Other Detail Items.
    $result2 = $query->execute();

    foreach ($result2 as $row) {
      $rows[] = [
        'data' => [
          // Cells.
          $this->formatDateWidLink($row),
          $this->formatLinkAndReferrer($row),
          $this->formatLookupLink($row),
        ],
        // Attributes for table row.
        'class' => ['grouper-row'],
      ];
    }

    $build['quantity'] = [
      '#children' => $quantity,
    ];

    $build['message'] = [
      '#children' => $message,
    ];

    $build['dblog_top_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No log messages available.'),
      '#attached' => [
        'library' => ['dblog/drupal.dblog'],
      ],
    ];

    $build['dblog_top_pager'] = ['#type' => 'pager'];

    return $build;

  }

  /**
   * Formats a The Event Tima and Event Link.
   *
   * @param object $row
   *   The record from the watchdog table. The object properties are: wid, uid,
   *   severity, type, timestamp, message, variables, link, name.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup|false
   *   The formatted log message or FALSE if the message or variables properties
   *   are not set.
   */
  public function formatDateWidLink($row) {
    $link_text = date("m/d/Y - H:i", $row->timestamp);
    $target_path = "/admin/reports/dblog/event/$row->wid/";
    $title = 'Click To View Event';

    $link = "<a href=\"$target_path\" class=\"wid-link\" title=\"$title\" target=\"_blank\">$link_text</a>";
    return ['data' => ['#markup' => $link]];
  }

  /**
   * Formats a Link and referrer.
   *
   * @param object $row
   *   The record from the watchdog table. The object properties are: wid, uid,
   *   severity, type, timestamp, message, variables, link, name.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup|false
   *   The formatted log message or FALSE if the message or variables properties
   *   are not set.
   */
  public function formatLinkAndReferrer($row) {
    $link = "<a href=\"$row->location\" class=\"wid-link\" title=\"REFERRER $row->referer\" target=\"_blank\">$row->location</a>";
    return ['data' => ['#markup' => $link]];
  }

  /**
   * Formats a Link to the page where the original log message resides.
   *
   * @param object $row
   *   The record from the watchdog table. The object properties are: wid, uid,
   *   severity, type, timestamp, message, variables, link, name.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup|false
   *   The formatted log message or FALSE if the message or variables properties
   *   are not set.
   */
  public function formatLookupLink($row) {
    $link = "<a href=\"/admin/reports/grouper/lookup/$row->wid\" class=\"lookup-link\" title=\"Find in dblog. Look for ITEM-NUMBER in url.\" target=\"_blank\"><span>&telrec;</span></a>";
    return ['data' => ['#markup' => $link]];
  }

  /**
   * Gets the query string args and forms a query string.
   *
   * @return string
   *   Return Query string.
   */
  public function getQueryString() {
    $data = [];

    if (isset($_GET['severity'])) {
      $data['severity'] = $_GET['severity'];
    }

    if (isset($_GET['begin-m-wid']) && !empty($_GET['begin-m-wid']) && is_numeric($_GET['begin-m-wid'])) {
      $data['begin-m-wid'] = $_GET['begin-m-wid'];
    }

    if (isset($_GET['begin-wid']) && !empty($_GET['begin-wid']) && is_numeric($_GET['begin-wid'])) {
      $data['begin-wid'] = $_GET['begin-wid'];
    }

    if (isset($_GET['after']) && !empty($_GET['after']) && is_numeric($_GET['after'])) {
      $data['after'] = $_GET['after'];
    }

    if (isset($_GET['end-m-wid']) && !empty($_GET['end-m-widd']) && is_numeric($_GET['end-m-widd'])) {
      $data['end-m-wid'] = $_GET['end-m-wid'];
    }

    if (isset($_GET['end-wid']) && !empty($_GET['end-wid']) && is_numeric($_GET['end-wid'])) {
      $data['end-wid'] = $_GET['end-wid'];
    }

    if (isset($_GET['before']) && !empty($_GET['before']) && is_numeric($_GET['before'])) {
      $data['before'] = $_GET['before'];
    }

    if (isset($_GET['limit']) && !empty($_GET['limit']) && is_numeric($_GET['limit'])) {
      $data['limit'] = $_GET['limit'];
    }

    if (isset($_GET['user']) && !empty($_GET['user']) && is_numeric($_GET['user'])) {
      $data['user'] = $_GET['user'];
    }

    if (isset($_GET['uid']) && !empty($_GET['uid']) && is_numeric($_GET['uid'])) {
      $data['uid'] = $_GET['uid'];
    }

    if (isset($_GET['location'])) {
      $data['location'] = $_GET['location'];
    }

    if (count($data)) {
      return '?' . http_build_query($data);
    }

    return '';

  }

}
