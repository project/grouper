<?php

namespace Drupal\grouper\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GrouperDistributionController.
 */
class GrouperDistributionController extends ControllerBase {

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->database = $container->get('database');
    return $instance;
  }

  /**
   * Summary.
   *
   * @return string
   *   Return Hello string.
   */
  public function summary() {

    $query = "SELECT COUNT(`location`) AS `num_pages`, `type` , (COUNT(`location`) * 100 / (SELECT COUNT(*) FROM `watchdog`)) as `percentage` FROM `watchdog` GROUP BY `type` ORDER BY `num_pages` DESC";
    $result = $this->database->query($query);

    $header = ['Number of Pages', 'Type of Message', 'Percentage'];
    $rows = [];

    foreach ($result as $record) {

      $rows[] = [$this->formatLink($record->num_pages, $record->type),
        $this->formatLink($record->type, $record->type),
        $this->formatLink($record->percentage . ' %', $record->type),
      ];

    }

    $form['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    return $form;

  }

  /**
   * Formats a Link.
   */
  public function formatLink($title, $type) {
    if (strcmp($type, 'php') == 0) {
      $target_path = '/admin/reports/grouper/php-summary/';
    }
    else {
      $target_path = '/admin/reports/grouper/summary?type=' . urlencode($type);
    }
    $link = "<a href=\"$target_path\" class=\"message-link\" title=\"Click For Summary\"target=\"_blank\">$title</a>";
    return ['data' => ['#markup' => $link]];
  }

}
