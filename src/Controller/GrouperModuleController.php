<?php

namespace Drupal\grouper\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GrouperModuleController.
 */
class GrouperModuleController extends ControllerBase {

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->database = $container->get('database');
    return $instance;
  }

  /**
   * Summary.
   *
   * @return string
   *   Return Hello string.
   */
  public function summary() {

    $connection = $this->database;

    $header = [
      ['data' => $this->t('Count'), 'field' => 'count', 'sort' => 'desc'],
      ['data' => $this->t('Module'), 'field' => 'location'],
    ];

    $count_query = $connection->select('watchdog');
    $count_query->addExpression('COUNT(DISTINCT(message))');

    $query = $connection->select('watchdog', 'w')
      ->extend('\Drupal\Core\Database\Query\PagerSelectExtender')
      ->extend('\Drupal\Core\Database\Query\TableSortExtender');
    $query->addExpression('COUNT(wid)', 'count');

    $query->fields('w', ['wid', 'module']);

    if (isset($_GET['type']) && !empty($_GET['type']) && $_GET['type'] == 'other') {
      $query->condition('w.type', 'php', '!=');
    }
    else {
      $query->condition('w.type', 'php', '=');
    }

    $query->groupBy('module');
    $query->distinct();
    $query->orderByHeader($header);
    $query->setCountQuery($count_query);
    $query->limit(30);

    /*
    // For Debugging.
    \Drupal::messenger()->addStatus($query->__toString());
    \Drupal::messenger()->addStatus(print_r($query->arguments(),1));
     */

    $result = $query->execute();

    $rows = [];
    foreach ($result as $row) {
      $rows[] = [$this->formatQuantity($row), $this->formatLocation($row)];
    }

    $build['dblog_top_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No log messages available.'),
      '#attached' => [
        'library' => ['dblog/drupal.dblog'],
      ],
    ];
    $build['dblog_top_pager'] = ['#type' => 'pager'];

    return $build;

  }

  /**
   * Formats a quantity message.
   *
   * @param object $row
   *   The record from the watchdog table.
   *   The object properties are: message, variables, count.
   */
  public function formatQuantity($row) {

    $target_path = "/admin/reports/grouper/php-summary/?module=" . $row->module;

    $link = "<a href=\"$target_path\" class=\"nid-link\" target=\"_blank\">$row->count</a>";

    return ['data' => ['#markup' => $link]];
  }

  /**
   * Formats a NAME message.
   *
   * @param object $row
   *   The record from the watchdog table.
   */
  public function formatLocation($row) {

    $target_path = "/admin/reports/grouper/php-summary/?module=" . $row->module;

    $link = "<a href=\"$target_path\" class=\"nid-link\" target=\"_blank\">$row->module</a>";

    return ['data' => ['#markup' => $link]];
  }

}
