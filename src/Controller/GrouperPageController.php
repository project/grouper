<?php

namespace Drupal\grouper\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GrouperPageController.
 */
class GrouperPageController extends ControllerBase {

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->database = $container->get('database');
    return $instance;
  }

  /**
   * Summary.
   *
   * @return string
   *   Return Hello string.
   */
  public function summary() {

    $connection = $this->database;

    $header = [
      ['data' => $this->t('Count'), 'field' => 'count', 'sort' => 'desc'],
      ['data' => $this->t('Location'), 'field' => 'location'],
    ];

    $count_query = $connection->select('watchdog');
    $count_query->addExpression('COUNT(DISTINCT(message))');

    $query = $connection->select('watchdog', 'w')
      ->extend('\Drupal\Core\Database\Query\PagerSelectExtender')
      ->extend('\Drupal\Core\Database\Query\TableSortExtender');
    $query->addExpression('COUNT(wid)', 'count');

    $query->fields('w', ['location']);

    if (isset($_GET['type']) && !empty($_GET['type']) && $_GET['type'] == 'other') {
      $query->condition('w.type', 'php', '!=');
    }
    else {
      $query->condition('w.type', 'php', '=');
    }

    $query->groupBy('location');
    $query->distinct();
    $query->orderByHeader($header);
    $query->setCountQuery($count_query);
    $query->limit(30);

    /*
    // for ebugging.
    \Drupal::messenger()->addStatus($query->__toString());
    \Drupal::messenger()->addStatus(print_r($query->arguments(),1));
     */

    $result = $query->execute();

    $rows = [];
    foreach ($result as $row) {
      $rows[] = [$this->formatQuantity($row), $this->formatLocation($row)];
    }

    $build['pre-table'] = [
      '#children' => $this->renderFilterForm(),
    ];

    $build['dblog_top_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No log messages available.'),
      '#attached' => [
        'library' => ['dblog/drupal.dblog'],
      ],
    ];
    $build['dblog_top_pager'] = ['#type' => 'pager'];

    return $build;

  }

  /**
   * Formats a quantity message.
   *
   * @param object $row
   *   The record from the watchdog table.
   *   The object properties are: message, variables, count.
   */
  public function formatQuantity($row) {
    if (isset($_GET['type']) && !empty($_GET['type']) && $_GET['type'] == 'other') {
      $target_path = "/admin/reports/grouper/summary/" . '?location=' . $row->location;
    }
    else {
      $target_path = "/admin/reports/grouper/php-summary" . '?location=' . $row->location;
    }

    $link = "<a href=\"$target_path\" class=\"nid-link\" target=\"_blank\">$row->count</a>";
    return ['data' => ['#markup' => $link]];
  }

  /**
   * Formats a NAME message.
   *
   * @param object $row
   *   The record from the watchdog table.
   */
  public function formatLocation($row) {

    $target_path = $row->location;

    $link = "<a href=\"$target_path\" class=\"nid-link\" target=\"_blank\">$row->location</a>";

    return ['data' => ['#markup' => $link]];
  }

  /**
   * Renders the Filters Form.
   *
   * @return string
   *   Return Error Type select list markup.
   */
  public function renderFilterForm() {
    $form_markup = '<form action="" METHOD="GET">';

    $form_markup .= '<table style="max-width: 400px;">';
    $form_markup .= '<tr>';
    $form_markup .= '<td>' . $this->renderTypeSelect(isset($_GET['type']) && !empty($_GET['type']) ? $_GET['type'] : '') . '</td>';
    $form_markup .= '<td valign="bottom"><input id="filter-submit" type="submit" value="Filter" size="12" style="height: 22px; padding-left: 10px; padding-right: 10px;"></td>';
    $form_markup .= '<td valign="bottom"><input type="reset" value="reset" style="height: 22px; padding-left: 10px; padding-right: 10px;"></td>';
    $form_markup .= '</tr>';
    $form_markup .= '</table>';
    $form_markup .= '</form>';
    return $form_markup;
  }

  /**
   * Type Select.
   *
   * @return string
   *   Return Type.
   */
  public function renderTypeSelect($type = 'PHP') {
    $types_arr = [
      'PHP',
      'other',
    ];

    $markup = '<label for="select-type">Type</label>
          <select data-drupal-selector="edit-severity"
                  name="type"
                  id="select-type"
                  size="1"
                  class="form-select">';

    foreach ($types_arr as $key => $arr_type) {
      if ($type == $arr_type) {
        $markup .= "<option selected=\"1\" value=\"$arr_type\">$arr_type</option>";
      }
      else {
        $markup .= "<option value=\"$arr_type\">$arr_type</option>";
      }
    }

    $markup .= '</select></span>';
    return $markup;

  }

}
