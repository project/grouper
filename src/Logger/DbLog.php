<?php

namespace Drupal\grouper\Logger;

use Drupal\dblog\Logger\DbLog as BaseDbLog;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Component\Utility\Xss;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Logs events in the watchdog database table.
 */
class DbLog extends BaseDbLog {

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []) {
    // Remove backtrace and exception since they may contain an unserializable variable.
    unset($context['backtrace'], $context['exception']);

    // Convert PSR3-style messages to \Drupal\Component\Render\FormattableMarkup
    // style, so they can be translated too in runtime.
    $message_placeholders = $this->parser->parseMessagePlaceholders($message, $context);

    try {
      $this->connection
        ->insert('watchdog')
        ->fields([
          'uid' => $context['uid'],
          'type' => mb_substr($context['channel'], 0, 64),
          'message' => $message,
          'variables' => serialize($message_placeholders),
          'severity' => $level,
          'link' => $context['link'],
          'location' => $context['request_uri'],
          'referer' => $context['referer'],
          'hostname' => mb_substr($context['ip'], 0, 128),
          'timestamp' => $context['timestamp'],
          'issue' => $this->getIssue($message, $message_placeholders),
          'module' => $this->getModuleName($message, $message_placeholders),
        ])
        ->execute();
    }
    catch (\Exception $e) {
      // When running Drupal on MySQL or MariaDB you can run into several errors
      // that corrupt the database connection. Some examples for these kind of
      // errors on the database layer are "1100 - Table 'xyz' was not locked
      // with LOCK TABLES" and "1153 - Got a packet bigger than
      // 'max_allowed_packet' bytes". If such an error happens, the MySQL server
      // invalidates the connection and answers all further requests in this
      // connection with "2006 - MySQL server had gone away". In that case the
      // insert statement above results in a database exception. To ensure that
      // the causal error is written to the log we try once to open a dedicated
      // connection and write again.
      if (
        // Only handle database related exceptions.
        ($e instanceof DatabaseException || $e instanceof \PDOException) &&
        // Avoid an endless loop of re-write attempts.
        $this->connection->getTarget() != self::DEDICATED_DBLOG_CONNECTION_TARGET
      ) {
        // Open a dedicated connection for logging.
        $key = $this->connection->getKey();
        $info = Database::getConnectionInfo($key);
        Database::addConnectionInfo($key, self::DEDICATED_DBLOG_CONNECTION_TARGET, $info['default']);
        $this->connection = Database::getConnection(self::DEDICATED_DBLOG_CONNECTION_TARGET, $key);
        // Now try once to log the error again.
        $this->log($level, $message, $context);
      }
      else {
        throw $e;
      }
    }
  }

  /**
   * Gets the issue if this is a php meaasge.
   */
  public function getIssue($message, $variables) {
    // Check for required properties.
    if (!isset($message) ||
        !isset($variables['%function']) ||
        !isset($variables['%file']) ||
        !isset($variables['%line']) ||
        !isset($variables['@backtrace_string'])
        ) {
      return '';
    }

    $issue = '';

    // Messages without variables or user specified text.
    if ($variables === NULL) {
      $message = Xss::filterAdmin($message);
    }
    else {

      if (str_contains($message, '@backtrace_string.')) {
        $message_template = str_replace(' @backtrace_string.', '.', $message);

      }
      else {
        $message_template = $message;
      }

      $issue = strip_tags(new TranslatableMarkup(Xss::filterAdmin($message_template), $variables));

    }

    return $issue;

  }

  /**
   * Gets the module name if this is a php meaasge.
   */
  public function getModuleName($message, $variables) {
    // Check for required properties.
    if (!isset($variables['%function']) ||
        !isset($variables['%file']) ||
        !isset($variables['%line']) ||
        !isset($variables['@backtrace_string'])
       ) {
      return '';
    }

    $module_name = '';
    $path = (isset($variables['%file']) ? $variables['%file'] : '');

    if (!empty($path)) {
      $path_segments_arr = explode('/', $path);
      $count = count($path_segments_arr);

      if (array_search('modules', $path_segments_arr) !== FALSE) {
        // Non Core modules.
        if (array_search('custom', $path_segments_arr) !== FALSE) {
          $module_name = 'custom/';
        }
        elseif (array_search('contrib', $path_segments_arr) !== FALSE) {
          $module_name = 'contrib/';
        }
        elseif (array_search('core', $path_segments_arr) !== FALSE) {
          $module_name = 'core/';
        }
        else {
          $module_name = 'modules/';
        }

        // Determine the module name.
        $modules_index = array_search('modules', $path_segments_arr);
        if ($modules_index !== FALSE) {
          if ($modules_index + 1 < $count) {
            if (strcmp($path_segments_arr[$modules_index + 1], 'contrib') == 0 ||
                strcmp($path_segments_arr[$modules_index + 1], 'custom') == 0) {

              if ($modules_index + 2 < $count) {
                $module_name .= $path_segments_arr[$modules_index + 2];
              }
            }
            else {
              $module_name .= $path_segments_arr[$modules_index + 1];
            }
          }
        }
      }
      elseif (array_search('themes', $path_segments_arr) !== FALSE) {
        // Themes.
        $module_name = 'themes/' . $path_segments_arr[$count - 1];
      }
      elseif (array_search('core', $path_segments_arr) !== FALSE) {
        // Core.
        $module_name = 'core/' . $path_segments_arr[$count - 1];
      }
      elseif (array_search('vendor', $path_segments_arr) !== FALSE) {
        // Core.
        $module_name = 'vendor/' . $path_segments_arr[$count - 1];
      }
      else {
        // Unknown.
        $module_name = 'Unknown/' . $path_segments_arr[$count - 1];
      }
    }
    return $module_name;
  }

}
