<?php

namespace Drupal\grouper;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the logger.dblog service.
 */
class GrouperServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Overrides logger.dblog service to exclude some unwanted log messages.
    $container->getDefinition('logger.dblog')
      ->setClass('Drupal\grouper\Logger\DbLog');

  }

}
