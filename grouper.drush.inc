<?php

/**
 * @file
 * File grouper.drush.inc.
 */

/**
 * Implements hook_drush_command().
 */
function grouper_drush_command() {
  $items = [];

  $items['marker'] = [
    'description' => dt('Creates a marker in the error log.'),
    'arguments'   => [
      'arg1'    => dt('Name of marker.'),
    ],
    'examples' => [
      'Standard example' => 'marker',
      'Argument example' => 'workout M1',
    ],
    'aliases' => ['mrk'],
  ];

  $items['watchdog-trim-before'] = [
    'description' => dt('Clears all the log entries before the supplied message wid.'),
    'arguments'   => [
      'wid'    => dt('wid.'),
    ],
    'examples' => [
      'Standard example' => 'watchdog-trim-before 1234',
    ],
    'aliases' => ['wtb'],
  ];

  $items['watchdog-trim-after'] = [
    'description' => dt('Clears all the log entries after the supplied message wid.'),
    'arguments'   => [
      'wid'    => dt('wid.'),
    ],
    'examples' => [
      'Standard example' => 'watchdog-trim-after 1234',
    ],
    'aliases' => ['wta'],
  ];

  $items['watchdog-trim-range'] = [
    'description' => dt('Clears all the log entries between the supplied message wids.'),
    'arguments'   => [
      'starting-wid'    => dt('Starting wid.'),
      'ending-wid'    => dt('Ending wid.'),
    ],
    'examples' => [
      'Standard example' => 'watchdog-trim-range 1234, 4321',
    ],
    'aliases' => ['wtr'],
  ];

  return $items;
}

/**
 * Callback function for drush watchdog-trim-before - wtb.
 *
 * $arg1
 *   An optional argument.
 */
function drush_grouper_marker($marker_name) {

  if (empty($marker_name)) {
    $marker_name = date("h:i : s a S");
  }
  else {
    $qty = \Drupal::database()->query("SELECT COUNT(*) FROM `watchdog` WHERE `type` = 'marker' AND `message` = :message", [":message" => 'MARKER - ' . $marker_name])->fetchField();

    if ($qty > 0) {
      print "\r\nThat Marker Name is Already Being Used. Please Choose a Unique Name.\r\n";
      return;
    }
  }

  \Drupal::logger('marker')->notice('MARKER - ' . $marker_name);
  print "\r\nMARKER - `$marker_name` was created at: " . date("Y/m/d H:i:s") . "\r\n";
}

/**
 * Callback function for drush watchdog-trim-before - wtb.
 *
 * $wid
 *   Required numeric wid to delete before.
 */
function drush_grouper_watchdog_trim_before($wid = NULL) {
  if (!empty($wid) && is_numeric($wid)) {
    if (drush_confirm("  Are you sure you want to delete all log messages before wid $wid ?")) {

      db_query("DELETE FROM `watchdog` WHERE `wid` < :wid", ['wid' => $wid]);
      print "  Cleared messages below $wid\r\n";
    }
  }
  else {
    print "\r\n  You need to pass in the numeric wid to delete before.\r\n";
  }
}

/**
 * Callback function for drush watchdog-trim-after - wta.
 *
 * $wid
 *   Required numeric wid to delete after.
 */
function drush_grouper_watchdog_trim_after($wid = NULL) {
  if (!empty($wid) && is_numeric($wid)) {
    if (drush_confirm("  Are you sure you want to delete all log messages after wid $wid ?")) {

      db_query("DELETE FROM `watchdog` WHERE `wid` > :wid", ['wid' => $wid]);
      print "  Cleared messages above $wid\r\n";
    }
  }
  else {
    print "\r\n  You need to pass in the numeric wid to delete after.\r\n";
  }
}

/**
 * Callback function for drush watchdog-trim-range - wtr.
 *
 * $starting_wid
 *   Required numeric wid to start deletions.
 * $ending_wid
 *   Required numeric wid to end deletions.
 */
function drush_grouper_watchdog_trim_range($starting_wid = NULL, $ending_wid = NULL) {
  if (!empty($starting_wid) && is_numeric($starting_wid) && !empty($ending_wid) && is_numeric($ending_wid)) {
    if ($starting_wid < $ending_wid) {
      if (drush_confirm("  Are you sure you want to delete all log messages between wid $starting_wid and wid $ending_wid ?")) {

        db_query("DELETE FROM `watchdog` WHERE `wid` > :swid AND  `wid` < :ewid", ['swid' => $starting_wid, 'ewid' => $ending_wid]);
        print "  Cleared messages between $starting_wid and $ending_wid.\r\n";
      }
    }
    else {
      print "\r\n  The ending-wid needs to be greater than the starting-wid.\r\n";
    }

  }
  else {
    print "\r\n  You need to pass in numeric wids to delete between.\r\n";
  }
}
