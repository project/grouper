CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Operation
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------
Grouper Slicer and Dicer

Not to be confused with Bass O Matic 76

Grouper reports on php warnings and errors gathered by Drupals dblog watchdog module.
Without dblog being enabled this module will be of no use.

Grouper makes your error and message log easier to understand
by grouping together similar messages and presenting you
with a Summary list of issues and an indication of how prevalent each one is.
You can easily drill down into any issue to get more detailed information.

The Drupal dblog lets you consume the raw fire hose of data.
Grouper lets you sip the data you are looking for thru a straw.

Drupals' built in dblog  Error Log is available at
Reports | Recent Log Messages (/admin/reports/dblog)
It allows you to view a linear list of Log Messages.
You can filter your search by Message type: like php and Message severity.

You might have an issues on your site that generates an error on certain or every page.
Your log could contain hundreds or even thousands of messages about that same issue 
with the only difference being what page was being viewed at the time of the error.

Example Log Message from a group of 5909 similar messages:

Notice: Undefined property: stdClass::$search_query_url
in your_module_settings_alter() 
(line 145 of /code/web/modules/custom/your_module/your_module.module)

This Log entry is in the log 5,909 times thats over 118 pages of log listings

Grouper makes your error and message log easier to understand
by grouping together similar messages and presenting you
with a Summary list of Issues and an indication of how prevalent each one is.
You can easily drill down into any issue to get more detailed information.

Instead of 5,909 listings of the error Grouper lists a single Issue.

Count    Message
-----------------------------------------------------------------------------------
5909     Notice: Undefined property: stdClass::$search_query_url
         in your_modules_settings_alter()
         (line 145 of /code/web/modules/custom/your_module/your_module.module)
-----------------------------------------------------------------------------------

If you click the 5909 link you get a Summary Page listing the 5909 Log messages.
Theres a stack trace of the issue showing the calling history.
Theres also link to see the original message. 
As well as a link to the page that created the message you might use for reproducing the issue.

NEW FEATURES
Module Summary of php errors by module.
Severity Summary of php and other log entries.
Pages Summary of log Messages for each Page.
People Summary of log Messages for each User. 
Host Summary of log Messages for each User. 
Distribution Summary of alllog Messages. 
Ability to add Markers.
Ability to filter by Markers.
Ability to filter by message ID (wid) or date.
Ability to limit or increase the number of issues displayed.
Drush Command for Adding Markers.
Drush Commands for selectively deleting messages in the dblog without deleting all of them.
Compatible with Drush 8 | Drush 9 | Drush 10



REQUIREMENTS
------------
Drupal 8 or Drupal 9
Drupal dblog enabled.


INSTALLATION
------------
Install like any other Drupal Module.


CONFIGURATION
-------------
There is no configuration for the Grouper module.


OPERATION
---------
Go to the Admin Reports page and click Grouper.

PHP Summary Tab
The PHP Tab Summary consolidates similar php errors onto the same line and 
displays the number of times that error occurred.
Floating over the error message item displays the php backtrace of the error.

Clicking on a Quantity on the Summary displays list of the individual errors and the pages they occurred on on a detail page.
Floating over the links on the detail page link item displays the referring page info if available.
Clicking on a date on the detail page brings you to the blog page for that error.
The detail page shows a formatted backtrace of the calling stack that led up to the error.
As well asa link to the actual Log Message and a link to the page that caused the log entry for reproducing the issue.

Other Summary Tab
The Other Summary Tab consolidates similar any other type of log messages (non php) onto the same line and 
displays the number of times that message event occurred. Filters are provided to limit
the display to the different types of errors available or to limit the list to a particular severity level. 
Clicking on the Quantity link produces a detail page with a list of all the log messages in that group.

Pages Summary Tab
The pages Summary groups together and counts the log messages related to individual pages.
Clicking on a Quantity on the Summary Shows the Summary of PHP or Other Log Messages related to that page.

People Summary Tab
The pages Summary groups together and counts the log messages related to individual users.
Clicking on the Quantity or a users name or email on the Summary Shows the Summary of PHP or Other Log Messages related to that user. The Default is to show the PHP Errors if you want to see Other types of log messages use the Type popup menu to select Other then click the Filter button.

Distribution Tab
The Distribution Tab Summary shows the percentage of each type of error message on the site.
Clicking one of the message types brings you to a summary tab which displays the selected content.
The Default is to show the PHP Errors if you want to see Other types of log messages use the Type popup menu to select Other then click the Filter button.

TROUBLESHOOTING
---------------
If you don't have any php errors, it is a great thing.
You might not know you have any issues unless you visit every corner of your site.
You might try using the Workout module to help identify errors.
A workout run will load every page on your site and cause any php errors that happened while rendering pages will create new log entries in the drupal dblog.


Grouper reports on php warnings and errors gathered by Drupals dblog watchdog module.
Without dblog being enabled this module will be of no use.


FAQ
---

Q: Can Grouper replace the Drupal dblog module?

A: No Grouper enhances the dblog by organizing it's data.

Q: When should I use Grouper?

A: When you have some errors that look similar.
   When you have a lot of log entries to consolidate.
   When you are trying to track reproducible bugs.

Q: Does Grouper store anything in the database.

A: No It does all its magic by just querying the existing database.

Q: What are Workout and Exerciser.
   Workout is a module that loads every page on a site to log the errors produced on a page when producing and rendering it.
   Exerciser is a module that loads every page of a specific content types on a site to log the errors produced on a page when producing and rendering it.

Q: Why would I want to use Workout or Exerciser.

A: php errors may occur when simply rendering pages.
   When you can reload a specific page and generate an error you have a reproducible issue.
   Reporting that url to a developer is one of the first things they need to work on an issue. 
   Some pages may have little or no traffic so visiting every page can flush out issues that could be hidden and get them in the log.
   Once in the log they can be analyzed by Grouper with the aggregated with other log messages.
   
   


APPENDIX
--------
Using url query string to filter messages.


/admin/reports/grouper/php-summary
AND
/admin/reports/grouper/summary

Accept a query string to limit the log messages that are grouped and displayed.

type
The type of the message. 
/admin/reports/grouper/php-summary - Displays log messages of type php.
/admin/reports/grouper/summary - Displays log messages NOT of type php.


severity 
0 Emergency | 1 Alert | 2 Critical | 3 Error | 4 Warning | 5 Notice | 6 Info | 7 Debug |


begin-m-wid
begin-wid
The log message event id of the starting log message.


end-m-wid
end-wid
The log message event id of the ending log message.


before
The timestamp of the starting log message.


after
The timestamp of the ending log message.


limit
The number if items to display in the window

Look at the urls produced by the filters for examples.


__________________________________________________________________________________________
DRUSH COMMAND LINE INTERFACE
__________________________________________________________________________________________

marker
Adds a new marker log message to the end of the log.

Argument:                                  Marker message add.

Examples:
 drush x1                                  marker 'Before installing new modules.'                            



Aliases: mrk

__________________________________________________________________________________________
watchdog-trim-before
Clears all the log entries before the supplied message wid.

Argument:                                  wid

Examples:
 drush x1                                  watchdog-trim-before 1234                            



Aliases: wtb

__________________________________________________________________________________________

watchdog-trim-after
Clears all the log entries after the supplied message wid.

Argument:                                  wid

Examples:
 drush x1                                  watchdog-trim-after 1234                            



Aliases: wta

__________________________________________________________________________________________

watchdog-trim-range
Clears all the log entries between the supplied message wids.

Argument:                                  wid

Examples:
 drush x1                                  watchdog-trim-range 1234 4321                            



Aliases: wtr

__________________________________________________________________________________________



MAINTAINERS
-----------

Current maintainer:

 * Seth Snyder (tfa) - https://www.drupal.org/u/seth-snyder



